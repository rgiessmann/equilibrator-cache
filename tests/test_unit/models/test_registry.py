# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the Registry class behavior."""


from pathlib import Path

import pytest
import toml

from equilibrator_cache.models import registry as registry


root_dir = Path(__file__).parent.parent.parent
with (root_dir / "seeds" / "registries.toml").open() as handle:
    registries = toml.load(handle)["registries"]


@pytest.mark.parametrize(
    "kwargs",
    [
        {"namespace": "metanetx.chemical", "pattern": r"MNXM\d{3}"},
        {
            "name": "with name",
            "namespace": "metanetx.chemical",
            "pattern": r"MNXM\d{3}",
        },
        {
            "namespace": "metanetx.chemical",
            "pattern": r"MNXM\d{3}",
        },
        {
            "namespace": "metanetx.chemical",
            "pattern": r"MNXM\d{3}",
            "url": "https://identifiers.org/metanetx.chemical",
        },
        {"namespace": "chebi", "pattern": r"CHEBI:\d{3}", "is_prefixed": True},
    ],
)
def test___init__(kwargs):
    """Ensure initialization occurs as expected."""
    reg = registry.Registry(**kwargs)
    for key, value in kwargs.items():
        assert getattr(reg, key) == value


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        (
            {
                "namespace": "metanetx.chemical",
                "pattern": r"MNXM\d{3}",
            },
            "Registry(namespace=metanetx.chemical)",
        )
    ],
)
def test___repr__(kwargs, expected):
    """Ensure a consistent string representation."""
    reg = registry.Registry(**kwargs)
    assert repr(reg) == expected


@pytest.mark.parametrize(
    "kwargs, accession, is_valid",
    [
        (
            {
                "namespace": "metanetx.chemical",
                "pattern": r"^MNXM\d{3}$",
            },
            "MNXM001",
            True,
        ),
        (
            {
                "namespace": "metanetx.chemical",
                "pattern": r"^MNXM\d{3}$",
            },
            "MNXM0001",
            False,
        ),
    ],
)
def test_is_valid_accession(kwargs, accession, is_valid):
    """Verify the registry's identifier validation."""
    reg = registry.Registry(**kwargs)
    assert reg.is_valid_accession(accession) is is_valid


@pytest.mark.parametrize("kwargs", registries)
def test_registry_seeds(kwargs):
    registry.Registry(**kwargs)
