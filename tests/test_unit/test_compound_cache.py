# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest
from sqlalchemy import create_engine

from equilibrator_cache.compound_cache import CompoundCache
from equilibrator_cache.models import Base, Compound, CompoundIdentifier, Registry


@pytest.mark.parametrize("kwargs", [{"engine": create_engine("sqlite:///:memory:")}])
def test___init__(kwargs):
    Base.metadata.create_all(bind=kwargs["engine"])
    CompoundCache(**kwargs)


def test_all_compound_accessions(ccache):
    expected = [
        "C00001",
        "C00002",
        "C00008",
        "C00009",
        "C00080",
        "MNXM01",
        "MNXM2",
        "MNXM3",
        "MNXM7",
        "MNXM9",
    ]
    assert ccache.all_compound_accessions() == expected
    expected.reverse()
    assert ccache.all_compound_accessions(ascending=False) == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("C00001", True),
        ("C00008", True),
        ("C00020", False),
        ("C00301", False),
        ("C00666", False),
        (None, False),
    ],
)
def test_accession_exists(ccache, accession, expected):
    assert ccache.accession_exists(accession) == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("kegg.compound:C00001", {"H": 2, "O": 1, "e-": 10}),
        (
            "kegg.compound:C00008",
            {"N": 5, "C": 10, "H": 13, "O": 10, "P": 2, "e-": 220},
        ),
        (
            "kegg.compound:C00002",
            {"N": 5, "C": 10, "H": 13, "O": 13, "P": 3, "e-": 260},
        ),
    ],
)
def test_get_element_data_frame(ccache, accession, expected):
    compound = ccache.get_compound(accession)
    elements = ccache.get_element_data_frame([compound])
    assert elements[compound].to_dict() == expected


@pytest.mark.parametrize(
    "namespace, identifiers, is_inchi_key, total",
    [
        ("", [], False, 0),
        ("kegg.compound", [], False, 0),
        ("kegg.compound", ["C99999"], False, 0),
        ("kegg.compound", ["C00001"], False, 1),
        ("kegg.compound", ["C00001", "C00002", "C00008"], False, 3),
        ("kegg.compound", ["C00001", "C00002", "C00001"], False, 2),
        (
            "inchikey",
            [
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "WQZGKKKJIJFFOK-GASJEMHNSA-N",
                "XTWYTFMLZFPYCI-KQYNXXCUSA-K",
            ],
            True,
            2,
        ),
    ],
)
def test_get_compounds(ccache, namespace, identifiers, is_inchi_key, total):
    results = ccache.get_compounds(namespace, identifiers, is_inchi_key=is_inchi_key)
    assert len(results) == total
