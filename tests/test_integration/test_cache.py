# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest


@pytest.mark.parametrize(
    ("accession", "exists", "first_name", "first_accession"),
    [
        ("kegg.compound:C00001", True, "H2O", "metanetx.chemical:MNXM2"),
        ("kegg.compound:C00002", True, "ATP", "metanetx.chemical:MNXM3"),
        ("kegg.compound:C99999", False, None, None),
    ],
)
def test_get_compound(ccache, accession, exists, first_name, first_accession):
    c = ccache.get_compound(accession)
    if exists:
        assert c is not None
        assert c.get_common_name() == first_name
        assert c.get_accession() == first_accession
    else:
        assert c is None


@pytest.mark.parametrize(
    "inchi",
    [
        "InChI=1S/H2O/h1H2",
        "InChI=1S/H3O4P/c1-5(2,3)4/h(H3,1,2,3,4)/p-2",
    ],
)
def test_get_compound_by_inchi(ccache, inchi):
    c = ccache.get_compound_by_inchi(inchi)
    assert c.inchi == inchi


@pytest.mark.parametrize(
    "inchi_key",
    [
        "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
        "NBIIXXVUZAFLBC-UHFFFAOYSA-L",
    ],
)
def test_search_compound_by_inchi_key(ccache, inchi_key):
    result = ccache.search_compound_by_inchi_key(inchi_key)
    assert inchi_key in {cmpnd.inchi_key for cmpnd in result}


@pytest.mark.parametrize(
    ("accession", "water"),
    [
        ("kegg.compound:C00002", False),
        ("kegg.compound:C00001", True),
    ],
)
def test_is_water(ccache, accession, water):
    cpd = ccache.get_compound(accession)
    assert ccache.is_water(cpd) is water


@pytest.mark.parametrize(
    ("accession", "proton"),
    [
        ("kegg.compound:C00080", True),
        ("kegg.compound:C00001", False),
    ],
)
def test_is_proton(ccache, accession, proton):
    cpd = ccache.get_compound(accession)
    assert ccache.is_proton(cpd) is proton


@pytest.mark.parametrize(
    ("query", "expected", "score"),
    [
        ("ATP", "ZKHQWZAMYRWXGA-KQYNXXCUSA-J", 1.0),
        ("H2O", "XLYOFNOQVPJJNP-UHFFFAOYSA-N", 1.0),
    ],
)
def test_search(ccache, query, expected, score):
    compounds = ccache.search(query)
    assert len(compounds) > 0
    compound, match = compounds[0]
    # The search should have a perfect match.
    assert match == score
    assert compound.inchi_key == expected
