# The MIT License (MIT)
#
# Copyright (c) 2018, 2019 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018, 2019 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Test that the COBRA compatibility module is functional."""


from unittest.mock import Mock

import pytest

from equilibrator_cache import compatibility as compat


@pytest.mark.parametrize(
    "grouping, expected",
    [
        ({}, 0),
        (
            {
                "metanetx.chemical": [
                    (
                        Mock(
                            spec_set=("id", "annotation"),
                            annotation={"metanetx.chemical": "MNXM01"},
                        ),
                        "MNXM01",
                    )
                ]
            },
            1,
        ),
        (
            {
                "metanetx.chemical": [
                    (
                        Mock(
                            spec_set=("id", "annotation"),
                            annotation={"metanetx.chemical": "MNXM01"},
                        ),
                        "MNXM01",
                    )
                ],
                "inchikey": [
                    (
                        Mock(
                            spec_set=("id", "annotation"),
                            annotation={"inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N"},
                        ),
                        "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                    )
                ],
            },
            2,
        ),
    ],
)
def test_map_metabolites_to_compounds(ccache, grouping, expected):
    result = compat.map_metabolites_to_compounds(
        ccache,
        grouping,
        inchi_key_namespace="inchikey",
        inchi_namespace="inchi",
    )
    assert len(result) == expected


@pytest.mark.parametrize(
    "metabolites, expected",
    [
        ([], 0),
        (
            [
                Mock(
                    spec_set=("id", "annotation"),
                    annotation={"metanetx.chemical": "MNXM01"},
                )
            ],
            1,
        ),
        (
            [
                Mock(
                    spec_set=("id", "annotation"),
                    annotation={"metanetx.chemical": "MNXM01"},
                ),
                Mock(
                    spec_set=("id", "annotation"),
                    annotation={"inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N"},
                ),
            ],
            2,
        ),
    ],
)
def test_map_cobra_metabolites(ccache, metabolites, expected):
    result = compat.map_cobra_metabolites(ccache, metabolites)
    assert len(result) == expected
