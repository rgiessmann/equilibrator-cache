# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2019 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import socket
import sqlite3

import httpx
import pytest

from equilibrator_cache import create_compound_cache_from_zenodo
from equilibrator_cache.zenodo import (
    DEFAULT_COMPOUND_CACHE_SETTINGS,
    find_record_by_doi,
)


_original_socket = socket.socket


def interrupt(*args, **kwargs):
    raise ConnectionError("Disconnected")


@pytest.fixture(scope="function")
def disconnect():
    socket.socket = interrupt
    yield
    socket.socket = _original_socket


def test_create_compound_cache_from_zenodo_with_connection():
    pytest.skip("Temporarily skip Zenodo test while restructuring the schema.")
    create_compound_cache_from_zenodo()


@pytest.mark.filterwarnings("ignore:No connection")
def test_create_compound_cache_from_zenodo_without_connection(disconnect):
    pytest.skip("Temporarily skip Zenodo test while restructuring the schema.")
    create_compound_cache_from_zenod()


def test_zenodo_timeout():
    with pytest.raises(httpx.ConnectTimeout):
        with httpx.Client(
            base_url=DEFAULT_COMPOUND_CACHE_SETTINGS.url, timeout=1e-9
        ) as client:
            # Disable the wait time on retry attempts.
            find_record_by_doi.retry_with(wait=None)(client, "10.5281/zenodo.4128543")


def test_zenodo_missing_doi():
    with pytest.raises(httpx.HTTPStatusError):
        with httpx.Client(
            base_url=DEFAULT_COMPOUND_CACHE_SETTINGS.url, timeout=2.0
        ) as client:
            # Disable the wait time on retry attempts.
            find_record_by_doi.retry_with(wait=None)(client, "xxxx/y.yyyyy")
