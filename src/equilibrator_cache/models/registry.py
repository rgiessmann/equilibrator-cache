"""A compound registry module."""
# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
import re
from typing import Optional

from sqlalchemy import Boolean, Integer, String
from sqlalchemy.orm import Mapped, mapped_column, reconstructor

from . import Base
from .mixins import TimeStampMixin


logger = logging.getLogger(__name__)


class Registry(TimeStampMixin, Base):
    r"""
    Model a MIRIAM registry resource.

    Attributes
    ----------
    id : int
        The primary key in the table.
    name : str
        The registry's common name.
    namespace : str
        The MIRIAM namespace identifier, e.g., 'metanetx.chemical'.
    pattern : str
        The regular expression pattern to validate against identifiers in the
        registry, e.g., ``^(MNXM\d+|BIOMASS)$``.
    url : str
        The universal resource identifier (URI) for this resource, e.g.,
        'http://identifiers.org/metanetx.chemical/'.
    is_prefixed : bool
        Whether or not identifiers of this registry are prefixed with the
        ``namespace`` itself, e.g., 'CHEBI:52971'.

    """

    __tablename__ = "registries"

    # SQLAlchemy column descriptors.
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    namespace: Mapped[str] = mapped_column(
        String, nullable=False, index=True, unique=True
    )
    pattern: Mapped[str] = mapped_column(String, nullable=False)
    url: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    is_prefixed: Mapped[Optional[bool]] = mapped_column(
        Boolean, default=False, nullable=False
    )

    def __init__(self, **kwargs):
        """Compile the identifier pattern."""
        super().__init__(**kwargs)
        self.compiled_pattern = re.compile(self.pattern)

    @reconstructor
    def init_on_load(self):
        """Compile the identifier pattern on load from database."""
        self.compiled_pattern = re.compile(self.pattern)

    def __repr__(self) -> str:
        """Return a string representation of this object."""
        return f"{type(self).__name__}(namespace={self.namespace})"

    def is_valid_accession(self, accession: str):
        """Validate an identifier against the compiled pattern."""
        return self.compiled_pattern.match(accession) is not None
